gg hardhad ~tutorial ~better
ref https://hardhat.org/tutorial

> Hardhat Network, a `local` Ethereum network designed for development
so a dev can quickly run smartcontract code without worrying about 
00 prepare wallet
01 ask for free token from faucet

prerequisite
> write code in `javascript`
> operate a `terminal`
> use `git`
> understand the basics of `how smart contracts work`
> set up a coinbase or `metamask`

some application idea
> `Insurance claims`
> `Estate planning`

how do smart contracts work?
ref https://cointelegraph.com/learn/what-are-smart-contracts-a-beginners-guide-to-automated-agreements
> think smart contracts as digital “if-then” statements between two (or more) parties
> let’s say a market asks a farmer for 100 ears of corn. 
> the market will `lock funds into a smart contract` that can then be approved when the former delivers
> the contract is canceled and funds are reversed to the client if the farmer misses their deadline.

> Ethereum is what’s considered a `distributed state machine`, 
> containing what’s known as the Ethereum Virtual Machine (EVM)
